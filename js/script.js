$(function () {
    var ERROR_CLASS = 'error'
      , HELP_CLASS  = 'fest-form__error'
    ;

    var ERROR_MSG    = 'Памылка'
      , REQUIRED_MSG = 'Неабходна запоўніць'
      , EMAIL_MSG    = 'Памылка, няправільны адрэс'
    ;

    var FEST_BLOCK_CLASS = 'fest-block';

    var RESPONSE = '<h3>Ваша паведамленне адпраўлена!</h3>';

    function displayError($element, callback, message) {
        var $parent = $element.parent()
          , $helpBlock = $parent.find('.'+HELP_CLASS)
        ;
        message = message || ERROR_MSG;
        if (callback($element)) {
            $parent.addClass(ERROR_CLASS);
            $helpBlock.html(message);
            return true;
        } else {
            $parent.removeClass(ERROR_CLASS);
            $helpBlock.empty();
            return false;
        }
    }

    $('#form input, #form textarea').focus(function () {
        var $parent = $(this).parent()
          , $helpBlock = $parent.find('.'+HELP_CLASS)
        ;
        $parent.removeClass(ERROR_CLASS);
        $helpBlock.empty();
    });

    $('#form').submit(function (e) {
        var $themeInput = $('#theme')
          , $emailInput = $('#email')
          , $areaInput  = $('#area')
        ;

        var emailInputError = displayError(
            $emailInput,
            function ($el) {
                return !$el.val().trim();
            },
            REQUIRED_MSG
        );
        if (!emailInputError) {
            emailInputError = displayError(
                $emailInput,
                function ($el) {
                    var value = $el.val().trim();
                    var result = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(value);
                    return !result;
                },
                EMAIL_MSG
            );
        }

        var themeInputError = displayError(
            $themeInput,
            function ($el) {
                return !$el.val().trim();
            },
            REQUIRED_MSG
        );

        var areaInputError = displayError(
            $areaInput,
            function ($el) {
                return !$el.val().trim();
            },
            REQUIRED_MSG
        );

        if (emailInputError || themeInputError || areaInputError) {
            return false;
        } else {
            $('.'+FEST_BLOCK_CLASS).html(RESPONSE);
            // return true if form valid
            return true;
        }
    });
});
