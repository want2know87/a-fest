<?php
header("Content-Type: text/html;charset=utf-8");

$params = require_once 'params.php';

$adminEmail = $params['festEmail'];

$isPost = false;
if (
    isset($_POST['fest-theme']) && !empty($_POST['fest-theme'])
 && isset($_POST['fest-email']) && !empty($_POST['fest-email'])
 && isset($_POST['fest-area'])  && !empty($_POST['fest-area'])
) {
    $festTheme = stripslashes(trim(htmlspecialchars($_POST['fest-theme'])));
    $festEmail = stripslashes(trim(htmlspecialchars($_POST['fest-email'])));
    $festArea  = stripslashes(trim(htmlspecialchars($_POST['fest-area'])));
    $headers  = "MIME-Version: 1.0" . "\r\n";
    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
    $headers .= "From: {$festEmail}\r\nReply-To: {$festEmail}";
    mail($adminEmail, $festTheme, $festArea, $headers);
    $isPost = true;
}
?>
<!DOCTYPE html>
<html class="no-js" lang="ru">

<head>
    <meta charset="utf-8">
    <title>Да сустрэчы на А-fest 2017!</title>
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="/css/textarea.css">
    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body class="layout">
    <div class="page-content">
        <div class="page-wrapper">
            <div class="page-container">
                <div class="flex-block">
                    <div class="text">
                        Да сустрэчы на
                        <br>
                        <span> А-fest 2017! </span>
                    </div>
                    <div class="socials">
                        <div class="socials__wrapper">
                            <a href="https://vk.com/a_fest_by" target="_blank" class=""><img src="assets/img/content/vk.jpg" alt=""></a>
                            <a href="https://www.facebook.com/AFestBy/" target="_blank" class=""><img src="assets/img/content/fb.jpg" alt=""></a>
                        </div>
                        <div class="socials__text">
                            Мы ў сацсетках
                        </div>
                    </div>
                </div>
                <div class="fest-block">
                    <?php if (!$isPost): ?>
                    <form action="" id="form" class="fest-form" method="post" novalidate>
                        <div class="fest-form__title">
                            Зваротная сувязь
                        </div>
                        <div class="fest-form__fields">
                            <div class="fest-form__inputs">
                                <div class="fest-form__input">
                                    <label for="theme" class="fest-form__label">Тэма</label>
                                    <input type="text" id="theme" name="fest-theme" required>
                                    <div class="fest-form__error"></div>
                                </div>
                                <div class="fest-form__input">
                                    <label for="email" class="fest-form__label ">Электронная пошта</label>
                                    <input type="email" id="email" name="fest-email" required>
                                    <div class="fest-form__error"></div>
                                </div>
                            </div>
                            <div class="fest-form__area">
                                <label for="area" class="fest-form__label">Ваша паведамленне</label>
                                <textarea name="fest-area" id="area" class="fext-form__textarea" required></textarea>
                                <div class="fest-form__error"></div>
                            </div>
                        </div>
                        <div class="fest-form__submit">
                            <button type="submit" class="fest-form__btn" data-hover="Адправіць">
                                <span>Адправіць</span>
                            </button>
                        </div>
                    </form>
                    <?php else: ?>
                    <h3>Ваша паведамленне адпраўлена!</h3>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
    <footer class="page-footer"></footer>

    <script>
    (function(i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function() {

            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),

            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)

    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-97815410-2', 'auto');

    ga('send', 'pageview');
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="/js/script.js"></script>
</body>

</html>
